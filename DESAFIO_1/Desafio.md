##           BASE DE DATOS COLEGIO
### PROPOSITO: Diseño de un base de datos para Un Colegio
 ### FASE 1: Identificacion de Entidades y atributos 
 ###  1. Identifica las entidades principales en el sistema de gestión del colegio. 
  Ayuda: Estudiante, Profesor, ...
  ### ENTIDADES
  ### ESTUDIANTE, PREFESOR, CURSO, ASIGNATURA, NOTA, HORARIO. 
   
 ### 2. Para cada entidad identificada, determina sus atributos. 
    Ayuda: entidad Estudiante -> atributos = ID de estudiante, Nombre, Apellido, Fecha de nacimiento, ...
### ESTUDIANTE: (ID de estudiante, Nombre, Apellido, Fecha de nacimiento, direccion, telefono, email ,curso actual. lista de cursos, notas)
### PROFESOR: ( Id de profesor, nombre, Apellido, fecha de nacimiento, genero, dirección, telefono, email. Asignatura, Horario de clases, cursos a cargo)
### CURSO: (Id de curso, nombre de curso, años academico, Nivel de estudio, lista de estudiantes escrito, Lista de asignaturas asociadas)
### ASIGNATURA: ( ID DE ASIGNATURA, Descripcion, horario de clases, Profesores Asignados, Cursos que pertenece)
### NOTA:( id de nota Estudiante relacionado asignatura relacionado, valor de la nota, fecha de nota)
### HORARIO: ( id de horario, dia de la semana, hora de inicio, hora de finalizacion, clase o actividades asociada, Pofesor Asignado, Curso Relacionado)
 
### 1. Primera Forma Normal (1NF):
#### Para asegurarnos de que cada entidad esté en 1NF, debemos asegurarnos de que no haya atributos multivaluados y que cada atributo contenga un solo valor atómico.

### Entidad Estudiante:
#### ID de estudiante (clave principal)
#### Nombre Apellido, Fecha de nacimiento, Género, Dirección, Teléfono, Email, Curso actual
### Entidad Profesor:
#### ID de profesor (clave principal)
#### Nombre, Apellido, Fecha de nacimiento, Género, Dirección, Teléfono, Email, Entidad 
### Curso
#### ID de curso (clave principal)
#### Nombre del curso, Año académico, Nivel educativo, Entidad
### Asignatura:
#### ID de asignatura (clave principal)
#### Nombre de la asignatura, Descripción
### Entidad Nota:
#### ID de nota (clave principal)
#### Valor de la nota, Fecha de la nota
### Entidad Horario:
#### ID de horario (clave principal)
#### Día de la semana, Hora de inicio, Hora de finalización
### 2. Segunda Forma Normal (2NF):
#### En la segunda forma normal, cada atributo no clave debe depender completamente de la clave principal.

#### En este caso, todas las entidades tienen claves primarias simples, por lo que ya están en 2NF.

### 3. Tercera Forma Normal (3NF):
#### Para alcanzar la tercera forma normal, debemos eliminar cualquier dependencia transitiva entre los atributos no clave.

#### No hay dependencias transitivas en estas entidades, ya que los atributos no clave dependen directamente de la clave principal.

#### Por lo tanto, las entidades están en 3NF.
### Fase 3: Creación de Diagrama Entidad-Relación

#### 1. Utiliza Lucidchart u otra herramienta para crear un diagrama entidad-relación (ER).
#### En el diagrama, muestra las entidades identificadas en la Fase 1 y sus relaciones(cardinalidad)

![alt text](bd_RI.png)

### Fase 4: Creación de la Base de Datos en posgreSQL

#### 1. Utilizando la estructura definida en el diagrama ER y los principios de normalización, 
#### crea las tablas necesarias en posgreSQL.
#### 2. Define las claves primarias y foráneas, así como los tipos de datos de cada atributo.
#### Ayuda: utiza las sentencias CREATE TABLE para crear las tablas.

### -- Creación de tablas
#### CREATE TABLE Estudiante (
    ID_estudiante SERIAL PRIMARY KEY,
    Nombre VARCHAR(50),
    Apellido VARCHAR(50),
    Fecha_nacimiento DATE,
    Dirección VARCHAR(100),
    Teléfono VARCHAR(15),
    Email VARCHAR(100)
);

### CREATE TABLE Profesor (
    ID_profesor SERIAL PRIMARY KEY,
    Nombre VARCHAR(50),
    Apellido VARCHAR(50),
    Especialidad VARCHAR(100),
    Teléfono VARCHAR(15),
    Email VARCHAR(100)
);

### CREATE TABLE Curso (
    ID_curso SERIAL PRIMARY KEY,
    Nombre_curso VARCHAR(100),
    ID_profesor INT REFERENCES Profesor(ID_profesor)
);

### CREATE TABLE Asignatura (
    ID_asignatura SERIAL PRIMARY KEY,
    Nombre_asignatura VARCHAR(100),
    ID_curso INT REFERENCES Curso(ID_curso)
);

### CREATE TABLE Nota (
    ID_nota SERIAL PRIMARY KEY,
    ID_estudiante INT REFERENCES Estudiante(ID_estudiante),
    ID_asignatura INT REFERENCES Asignatura(ID_asignatura),
    Valor_nota DECIMAL(5,2) -- Suponiendo notas como decimales
);

### CREATE TABLE Horario (
    ID_horario SERIAL PRIMARY KEY,
    Hora_inicio TIME,
    Hora_fin TIME,
    Día_semana VARCHAR(10),
    ID_curso INT REFERENCES Curso(ID_curso),
    ID_profesor INT REFERENCES Profesor(ID_profesor),
    ID_asignatura INT REFERENCES Asignatura(ID_asignatura)
);

### Extra: Datos(Opcional)

#### 1. Con la estructura de la base de datos creada, puedes insertar datos de muestra.
#### Ayuda: INSERT INTO()values();
#### 2. Inserta datos en todas las tablas de la base de datos para probar su funcionamiento.

### -- Inserción de datos en la tabla Estudiante
#### INSERT INTO Estudiante (Nombre, Apellido, Fecha_nacimiento, Dirección, Teléfono, Email)
#### VALUES ('Juan', 'Pérez', '1998-05-15', 'Calle Principal 123', '123456789', 'juan@example.com'),
#### ('María', 'Gómez', '1999-08-22', 'Avenida Central 456', '987654321', 'maria@example.com');

### -- Inserción de datos en la tabla Profesor
#### INSERT INTO Profesor (Nombre, Apellido, Especialidad, Teléfono, Email)
#### VALUES ('Luis', 'Martínez', 'Matemáticas', '555123456', 'luis@example.com'),
#### ('Ana', 'Rodríguez', 'Historia', '555987654', 'ana@example.com');

### -- Inserción de datos en la tabla Curso
#### INSERT INTO Curso (Nombre_curso, ID_profesor)
#### VALUES ('Matemáticas Avanzadas', 1),
#### ('Historia del Arte', 2);

### -- Inserción de datos en la tabla Asignatura
#### INSERT INTO Asignatura (Nombre_asignatura, ID_curso)
#### VALUES ('Cálculo Integral', 1),('Renacimiento', 2);

### -- Inserción de datos en la tabla Nota
#### INSERT INTO Nota (ID_estudiante, ID_asignatura, Valor_nota)
#### VALUES (1, 1, 85.5),(1, 2, 90.0), (2, 1, 78.0),(2, 2, 88.5);

### -- Inserción de datos en la tabla Horario
#### INSERT INTO Horario (Hora_inicio, Hora_fin, Día_semana, ID_curso, ID_profesor, ID_asignatura)
#### VALUES ('08:00', '10:00', 'Lunes', 1, 1, 1),
#### ('10:30', '12:30', 'Miércoles', 1, 1, 1),
#### ('08:00', '10:00', 'Martes', 2, 2, 2),
#### ('10:30', '12:30', 'Jueves', 2, 2, 2);
